import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Fragment } from "react";
import { useState } from "react";

import Home from "./pages/Home";
import Header from "./components/Header/Header";
import FooterComponent from "./components/FooterComponent";
import About from "./pages/About";
import Form from "./pages/Form";
import FormAlvin from "./pages/Form/FormAlvin";

function App() {
  const [show, setShow] = useState(false);

  return (
    <Router>
      <Header />
      <Fragment>
        <Route path="/" exact component={Home}></Route>
        <Route path="/about" component={About}></Route>
        <Route path="/form" component={Form}></Route>
        <Route path="/formalvin" component={FormAlvin}></Route>
      </Fragment>
      <FooterComponent />
    </Router>
  );
}

export default App;
