import React, { Component } from 'react'

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { faFacebookSquare  } from "@fortawesome/free-brands-svg-icons";
import { faTwitterSquare  } from "@fortawesome/free-brands-svg-icons";
import { faWhatsappSquare  } from "@fortawesome/free-brands-svg-icons";
import { faYoutubeSquare  } from "@fortawesome/free-brands-svg-icons";
import { faInstagramSquare  } from "@fortawesome/free-brands-svg-icons";
import { faDribbbleSquare  } from "@fortawesome/free-brands-svg-icons";
import { faLinkedin  } from "@fortawesome/free-brands-svg-icons";

import ThumbnailComponent from './components/ThumbnailComponent.js'
import QuoteComponent from './components/QuoteComponent.js'
import TabsComponent from './components/TabsComponent.js'
import FooterComponent from './components/FooterComponent.js'
import FeaturesComponent from './components/FeaturesComponent.js'

class DraftBalqis extends Component {

	render() {
		var tabData = [
        	{title: 'home', text: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit'}, 
        	{title: 'profile', text: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit'}
      	];
      	var featureData = [
      		{id: 1, icon:faHome, title:'Friendly Documentaton', subtitle:'Straight to the point'},
      		{id: 2, icon:faHeart, title:'Responsive Design', subtitle:'Acrossa all major devices'},
      		{id: 3, icon:faFacebookSquare, title:'Friendly Documentaton', subtitle:'Straight to the point'},
      		{id: 4, icon:faTwitterSquare, title:'Responsive Design', subtitle:'Acrossa all major devices'},
      		{id: 5, icon:faYoutubeSquare, title:'Friendly Documentaton', subtitle:'Straight to the point'},
      		{id: 6, icon:faLinkedin, title:'Responsive Design', subtitle:'Acrossa all major devices'}
      	];
		const colStyle = {height:'50px', marginTop: '130px'}
		const thumbnailStyle = {marginTop: '500px'}
	
		return (
			<>
			<Container>
			// Feautures Components DONE
      			<FeaturesComponent features={featureData} />

      		// Thumbnails Components DONE
      			<ThumbnailComponent imageSrc='/images/person3.png' imageAlt='person3' />
      			<ThumbnailComponent imageSrc='/images/person1.jpg' imageAlt='person3' />

      		// Quotes Components DONE
      			<QuoteComponent 
      				by='Catherine Pulsifer, Personal Development Goals'
      				quote='The first step to make a change in your life, to get what you want from life, to make your life better is to first specifically decide what it is you want. Unless you know what you want you will never arrive because you have no final destination.'
      			/>
      		// Tabs Components DONE
    			<TabsComponent tabs={tabData} />

    		</Container>
    		<FooterComponent />
    		</>
		);
	}
}

export default DraftBalqis;
