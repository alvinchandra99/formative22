import React, { Component } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import "./Card.css";

export default class Card extends Component {
  constructor() {
    super();
    this.state = {
      card: [],
    };
  }

  componentDidMount() {
    fetch("http://localhost:8081/getcard")
      .then((res) => res.json())
      .then((data) =>
        this.setState({
          card: data
        })
      );
  }

  render() {
    return (
      <>
        <Container id="cardheader">
          <h3>What We Do</h3>

          <Container fluid className="mt-3" id="cardcontent">
            <Row>
              {this.state.card.map((card) => {
                return (
                  <Col key={card.id} md={4}>
                    <h4>{card.title}</h4>
                    <p> {card.subtitle}</p>
                    <Button>Learn More</Button>
                  </Col>
                );
              })}
            </Row>
          </Container>
        </Container>
      </>
    );
  }
}
