import React, { Component } from 'react'

import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Tabs } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { faBullhorn } from "@fortawesome/free-solid-svg-icons";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { faChartLine } from "@fortawesome/free-solid-svg-icons";
import { faComments } from "@fortawesome/free-solid-svg-icons";

class FeaturesComponent extends Component {
  constructor() {
    super();
    this.state = {
      feature: [],
    };
  }

  componentDidMount() {
    const headers = { 'Content-Type': 'application/json' }
    fetch("http://localhost:8081/getFeatures", { headers })
      .then((res) => {
          if (res.ok) {
            return res.json()
          }
          throw res;
        })
      .then((data) =>
        this.setState({
          feature: data,
        })
        
      ).catch(error => {
          console.error("Error fetching data: ", error);
        });
  }

  render() {
    const colStyle = {height:'50px', marginTop: '130px'}
    const iconStyle = {height:'60px', width: '60px'}
    const pStyle = {color:'#b0b0b0'}

    const listFeature = this.state.feature.map((feature) => {
      console.log(feature)
      var temp;
      if (feature.icon == 'faBullhorn') {
        temp = faBullhorn;
      } else if (feature.icon == 'faHome') {
        temp = faHome;
      } else if (feature.icon == 'faHeart') {
        temp = faHeart;
      } else if (feature.icon == 'faComments') {
        temp = faComments;
      } else if (feature.icon == 'faChartLine') {
        temp = faChartLine;
      } else {
        temp = faCalendarAlt;
      }
      if (feature.id === 1) {
        return (<><Col sm={2} style={{marginTop: '20px'}}></Col>
          <Col sm={2} style={{marginTop: '20px'}}>
            <Row className='justify-content-md-center'>
              <FontAwesomeIcon icon={temp} style={iconStyle} />
            </Row>
            <Row className='justify-content-md-center'>
              <h4 className="text-center">{ feature.title }</h4>
            </Row>
            <Row className='justify-content-md-center'>
              <p className='text-center' style={pStyle}>{ feature.subtitle }</p>
            </Row>
          </Col></>)
      } else if (feature.id % 4 === 0) {
        return (<><Col sm={2} style={{marginTop: '20px'}}>
          <Row className='justify-content-md-center'>
            <FontAwesomeIcon icon={temp} style={iconStyle} />
          </Row>
          <Row className='justify-content-md-center'>
            <h4 className="text-center">{ feature.title }</h4>
          </Row>
          <Row className='justify-content-md-center'>
            <p className='text-center' style={pStyle}>{ feature.subtitle }</p>
          </Row>
        </Col>
        <Col sm={2} style={colStyle}></Col></>)
      } else {
      return (<><Col sm={2} style={{marginTop: '20px'}}>
        <Row className='justify-content-md-center'>
          <FontAwesomeIcon icon={temp} style={iconStyle} />
        </Row>
        <Row className='justify-content-md-center'>
          <h4 className="text-center">{ feature.title }</h4>
        </Row>
        <Row className='justify-content-md-center'>
          <p className='text-center' style={pStyle}>{ feature.subtitle }</p>
        </Row>
      </Col></>)}
    }
    );
    
    return (
      <Row className='justify-content-md-center'>
        {listFeature}
      </Row>
    );
  }


}

export default FeaturesComponent;
