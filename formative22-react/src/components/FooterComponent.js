import React, { Component } from 'react'

import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookSquare  } from "@fortawesome/free-brands-svg-icons";
import { faTwitterSquare  } from "@fortawesome/free-brands-svg-icons";
import { faWhatsappSquare  } from "@fortawesome/free-brands-svg-icons";
import { faYoutubeSquare  } from "@fortawesome/free-brands-svg-icons";
import { faInstagramSquare  } from "@fortawesome/free-brands-svg-icons";
import { faDribbbleSquare  } from "@fortawesome/free-brands-svg-icons";
import { faLinkedin  } from "@fortawesome/free-brands-svg-icons";

class FooterComponent extends Component {
  constructor(props)
  {
    super(props);
  }

  render() {
    const rowStyle = {backgroundColor: '#363636', height: '150px', color: 'white'}
    const colStyle = {height:'50px', marginTop: '20px'}
    const iconStyle = {height:'30px', width: '30px', color: 'white', marginRight: '5px'}

    return (
      <Row className='justify-content-md-center' style={rowStyle}>
        <Col sm={2} style={colStyle}></Col>
        <Col sm={8} style={colStyle}>
          <Row className='justify-content-md-center'>
            <Col sm={4}>
              <h5>ABOUT US</h5>
              <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
            </Col>
            <Col sm={4}>
              <h5>GET SOSIAL</h5>
              <Row>
                <FontAwesomeIcon icon={faFacebookSquare} style={iconStyle} />
                <FontAwesomeIcon icon={faTwitterSquare} style={iconStyle} />
                <FontAwesomeIcon icon={faWhatsappSquare} style={iconStyle} />
                <FontAwesomeIcon icon={faYoutubeSquare} style={iconStyle} />
                <FontAwesomeIcon icon={faInstagramSquare} style={iconStyle} />
                <FontAwesomeIcon icon={faDribbbleSquare} style={iconStyle} />
                <FontAwesomeIcon icon={faLinkedin} style={iconStyle} />
              </Row>
            </Col>
            <Col sm={4}>
              <h5>NEWSLETTER</h5>
              <Form>
                <Form.Row className="align-items-center">
                <Col sm={10}>
                  <Form.Label htmlFor="inlineFormInput" srOnly>Name</Form.Label>
                  <Form.Control className="mb-2" id="inlineFormInput" placeholder="Email" />
                </Col>
                <Col sm={2}>
                  <Button type="submit" className="mb-2">GO</Button>
                </Col>
              </Form.Row>
            </Form>
            </Col>
          </Row>
        </Col>
        <Col sm={2} style={colStyle}></Col>
      </Row>
    );
  }


}

export default FooterComponent;
