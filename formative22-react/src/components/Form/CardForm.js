import { Form } from "react-bootstrap";

const CardForm = () => {
  return (
    <Form className="mt-5">
      <Form.Group className="mb-3">
        <Form.Label>Title</Form.Label>
        <Form.Control id="cardTitle" type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Subtitle</Form.Label>
        <Form.Control id="cardSubTitle" type="text" placeholder="" />
      </Form.Group>
    </Form>
  );
};

export default CardForm;
