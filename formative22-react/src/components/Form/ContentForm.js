import { Form } from "react-bootstrap";

const ContentForm = () => {
  return (
    <Form className="mt-5">
      <Form.Group className="mb-3">
        <Form.Label>Title</Form.Label>
        <Form.Control id="contentTitle" type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Text</Form.Label>
        <Form.Control id="contentText" type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Type</Form.Label>
        <Form.Control id="contentType" as="select">
          <option value="main">Main Content</option>
          <option value="tab">Tab Content</option>
        </Form.Control>{" "}
      </Form.Group>
    </Form>
  );
};

export default ContentForm;
