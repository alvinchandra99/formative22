import { Form } from "react-bootstrap";

const FeatureForm = () => {
  return (
    <Form className="mt-5">
      <Form.Group className="mb-3">
        <Form.Label>Icon</Form.Label>
        <Form.Control id="featureIcon" type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Title</Form.Label>
        <Form.Control id="featureTitle" type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Subtitle</Form.Label>
        <Form.Control id="featureSubTitle" type="text" placeholder="" />
      </Form.Group>
    </Form>
  );
};

export default FeatureForm;
