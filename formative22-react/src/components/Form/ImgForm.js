import { Form } from "react-bootstrap";

const ImgForm = () => {
  return (
    <Form className="mt-5">
      <Form.Group className="mb-3">
        <Form.Label>Img Url</Form.Label>
        <Form.Control type="text" id="imgUrl" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Type</Form.Label>
        <Form.Control id="imgType" as="select">
          <option value="imgslider">Img Slider</option>
          <option value="imgthumbnail">Img Thumbnail</option>
        </Form.Control>{" "}
      </Form.Group>
    </Form>
  );
};

export default ImgForm;
