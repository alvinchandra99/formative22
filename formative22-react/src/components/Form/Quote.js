import React, { Component } from 'react'

import { Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';

class Quote extends Component {

	constructor(props) {
    	super(props);
    	this.state = {
      	selectValue: ""
    	};

    	this.handleDropdownChange = this.handleDropdownChange.bind(this);
  	}

  	handleDropdownChange(e) {
    	this.setState({ selectValue: e.target.value });
  	}

	render() {

		function submit() {
			//fetch
  		}

		return (
			<>
				<Row className='justify-content-md-center'>

            		<Form.Label><h4>Quote</h4></Form.Label>
            		<Form.Control as='textarea' rows={3} id='quote' name='quote' />

            		<Form.Label><h4>By</h4></Form.Label>
            		<Form.Control type='text' id='by' name='by' />

            		<Form.Label><h4>Type</h4></Form.Label>
            		<Form.Control as='select' id='type' name='type' onChange={this.handleDropdownChange}>
              		<option value='atas'>Atas</option>
              		<option value='bawah'>Bawah</option>
            		</Form.Control>

            		<br /><br /><br />
            		<Form.Control type='hidden' id='clue' name='clue' value={this.props.clue} />
            		<Button variant="primary" onClick={submit}>Submit</Button>{' '}
          		</Row>
    		</>
		);
	}
}

export default Quote;
