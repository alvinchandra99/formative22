import { Form } from "react-bootstrap";

const QuoteForm = () => {
  return (
    <Form className="mt-5">
      <Form.Group className="mb-3">
        <Form.Label>Quote</Form.Label>
        <Form.Control id="quote" type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>By</Form.Label>
        <Form.Control id="quoteby" type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Type</Form.Label>
        <Form.Control id="quoteType" as="select">
          <option>Home</option>
          <option>About</option>
        </Form.Control>{" "}
      </Form.Group>
    </Form>
  );
};

export default QuoteForm;
