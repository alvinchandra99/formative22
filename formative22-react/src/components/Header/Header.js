import React, { Component } from "react";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import { Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Header.css";

export default class Header extends Component {
  render() {
    return (
      <>
        <Navbar className="navbar" expand="lg">
          <Container fluid>
            <Navbar.Brand href="#home">Nexsoft Design Task</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#Portofolio">Portofolio</Nav.Link>
                <Nav.Link href="#Blog">Blog</Nav.Link>
                <Nav.Link href="#Pages">Pages</Nav.Link>
                <Nav.Link href="#home">Features</Nav.Link>
                <Nav.Link href="#link">Contact</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>{" "}
      </>
    );
  }
}
