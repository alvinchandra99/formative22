import React, { Component } from "react";
import { Container, Carousel } from "react-bootstrap";
import "./ImgSlider.css";

export default class ImgSlider extends Component {
  constructor() {
    super();
    this.state = {
      imgslider: [],
    };
  }

  componentDidMount() {
    fetch(
      "http://localhost:8081/getimages?" +
        new URLSearchParams({
          type: "imgslider",
        })
      ).then((res) => {
          if (res.ok) {
            return res.json()
          }
          throw res;
        })
      .then((data) =>
        this.setState({
          imgslider: data,
        })
        
      ).catch(error => {
          console.error("Error fetching data: ", error);
        });
  }

  render() {
    return (
      <>
        <Carousel>
          {this.state.imgslider.map((data) => {
            console.log(data)
            return (
              <Carousel.Item>
                <Container className="dareto">
                  <h3>Dare to</h3>
                </Container>
                <Container className="dream">
                  <h1>Dream</h1>
                </Container>
                <img className="img-fluid w-100" src={data.imgUrl} alt="First slidae" />
              </Carousel.Item>
            );
          })}
        </Carousel>
      </>
    );
  }
}
