import React, { Component, Fragment } from "react";
import "./MainContent.css";
export default class MainContent extends Component {
  constructor() {
    super();
    this.state = {
      content: [],
    };
  }
  componentDidMount() {
    fetch(
      "http://localhost:8081/getcontent?" +
        new URLSearchParams({
          type: "maincontent",
        })
    )
      .then((res) => res.json())
      .then((data) =>
        this.setState({
          content: data,
        })
      );
  }
  render() {
    return (
      <>
        {this.state.content.map((content) => {
          return (
            <Fragment key={content.id}>
              <div id="strip"></div>
              <h5 id="maincontent-title">{content.title}</h5>
              <p>{content.text}</p>
            </Fragment>
          );
        })}
      </>
    );
  }
}
