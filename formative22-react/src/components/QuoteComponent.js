import React, { Component } from 'react'
import '../index.css';

import { Row } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

class QuoteComponent extends Component {
  constructor() {
    super();
    this.state = {
      quote: []
    };
  }

  componentDidMount() {
    fetch(
      "http://localhost:8081/getcontent?" +
        new URLSearchParams({
          type: this.props.page,
        })
      ).then((res) => {
          if (res.ok) {
            return res.json()
          }
          throw res;
        })
      .then((data) =>
        this.setState({
          quote: data,
        })
        
      ).catch(error => {
          console.error("Error fetching data: ", error);
        });
  }

  render() {
    const cardStyle = {backgroundColor: '#ededed'}
    const rowStyle = {marginTop: '20px', marginBottom: '20px'}

    return (
      <>
      {this.state.quote.map((data) => {
        console.log(data)
          return (
      <Row className='justify-content-md-center' style={rowStyle} >
        <Card text="black" style={cardStyle}>
          <Card.Body> 
            <Card.Text>
              {data.quote}
            </Card.Text>
            <Card.Title className='text-right'><h5><i>{data.by}</i></h5></Card.Title>
          </Card.Body>
        </Card>
      </Row>
        );
      })}
      </>
    );
  }


}

export default QuoteComponent;
