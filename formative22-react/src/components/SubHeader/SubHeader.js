import React, { Component } from "react";
import { Container } from "react-bootstrap";
import "./SubHeader.css";

export default class SubHeader extends Component {
  render() {
    return (
      <>
        <Container fluid id="subheader">
          <h6>"Vision is the art of seeing what is invisible to others." -Jonathan Swift"</h6>
        </Container>
      </>
    );
  }
}
