import React, { Component } from 'react'
import { Tabs } from 'react-bootstrap';

class TabsComponent extends Component {
  constructor(props, context) {
    super(props, context);
      this.state = {
        key: '',
        tab: []
      };
  }

  componentDidMount() {
    fetch(
      "http://localhost:8081/getcontent?" +
        new URLSearchParams({
          type: "tab",
        })
      ).then((res) => {
          if (res.ok) {
            return res.json()
          }
          throw res;
        })
      .then((data) =>
        this.setState({
          tab: data,
        })
        
      ).catch(error => {
          console.error("Error fetching data: ", error);
        });
  }

  render() {
    const listTab = this.state.tab.map((data) => {
      console.log(data)
      if (data.id == 1) {this.state.key = data.title}
      return (<Tabs.Tab eventKey={data.title} title={data.title}>
        {data.text}
      </Tabs.Tab>);}
    );
    
    return (
      <Tabs
        id="controlled-tab"
        activeKey={this.state.key}
        onSelect={key => this.setState({ key })}
      >
        {listTab}
      </Tabs>
    );
  }


}

export default TabsComponent;
