import React, { Component } from 'react'

import { Row } from 'react-bootstrap';

import ImageShadow from 'react-image-shadow';
import 'react-image-shadow/assets/index.css';

class ThumbnailComponent extends Component {
  constructor() {
    super();
    this.state = {
      thumbnail: [],
    };
  }

  componentDidMount() {
    fetch(
      "http://localhost:8081/getimages?" +
        new URLSearchParams({
          type: "thumbnail",
        })
      ).then((res) => {
          if (res.ok) {
            return res.json()
          }
          throw res;
        })
      .then((data) =>
        this.setState({
          thumbnail: data,
        })
        
      ).catch(error => {
          console.error("Error fetching data: ", error);
        });
  }

  render() {
    const imgStyle = {width: '300px', borderRadius: 10}
    const thumbnailStyle = {marginTop: '20px', marginBottom: '10px'}

    return (
      <Row className='justify-content-md-center' style={thumbnailStyle} >
        {this.state.thumbnail.map((thumbnail) => {
          console.log(thumbnail)
          return (
            <ImageShadow src={thumbnail.imgUrl} style={imgStyle} />
            );
        })}
      </Row>
    );
  }


}

export default ThumbnailComponent;
