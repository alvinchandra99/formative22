import React, { Component } from 'react'

import { Container } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';

import ThumbnailComponent from '../components/ThumbnailComponent.js'
import QuoteComponent from '../components/QuoteComponent.js'
import MainContent from '../components/MainContent/MainContent.js'
import TabsComponent from '../components/TabsComponent.js'

class About extends Component {
  constructor(props)
  {
    super(props);
  }

  render() {
  	var tabData = [
        	{title: 'home', text: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit'}, 
        	{title: 'profile', text: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit'}
      	];

    const subHeader = { backgroundColor: '#02a193', color: 'white', height: '50px', verticalAlign: 'middle' }
    const leftSide = { marginRight: '65px', marginTop: '20px', marginBottom: '20px' }
    const rightSide = { marginLeft: '25px' }

    return (
    	<>
    	<Row className='justify-content-md-center' style={subHeader}>
    		<Col sm={2}></Col>
    		<Col sm={4} style={{ marginTop: '10px' }}><p>ABOUT</p></Col>
    		<Col sm={4} style={{ marginTop: '10px', textAlign: 'right' }}><p>Meet Our Team</p></Col>
    		<Col sm={2}></Col>
    	</Row>
      	<Container>
      		<Row className='justify-content-md-center'>
      			<Col sm={1}></Col>
      			<Col sm={5} style={leftSide}>
      				<MainContent />
      				<QuoteComponent page='about'/>
      				<TabsComponent tabs={tabData} />
      			</Col>
      			<Col sm={3} style={rightSide}>
      				<ThumbnailComponent />
      			</Col>
      			<Col sm={2}></Col>
      		</Row>
      	</Container>
      	</>
    );
  }


}

export default About;
