import React, { Component } from 'react'

import { Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';

import Quote from '../components/Form/Quote'

class FormComponent extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      selectValue: ""
    };

    this.handleDropdownChange = this.handleDropdownChange.bind(this);
  }

  handleDropdownChange(e) {
    this.setState({ selectValue: e.target.value });
    this.changeDiv(e.target.value);
  }

  changeDiv(temp) {
    if (temp == 1) { // quote
      document.getElementById("forms").innerHTML = "<Quote clue={temp} />"; 
    } else if (temp == 2) { // feature
      document.getElementById("forms").innerHTML = '<br><p>2</p>'; 
    } else if (temp == 3) { // card
      document.getElementById("forms").innerHTML = '<br><p>3</p>';
    } else if (temp == 4) { // image
      document.getElementById("forms").innerHTML = '<br><p>3</p>';
    } else if (temp == 5) { // content
      document.getElementById("forms").innerHTML = '<br><p>3</p>';
    }
  }

  render() {

    function submit() {
      if (document.getElementById('clue').value == 1) { // quote
        document.getElementById("demo").innerHTML = '';
        // fetch
      } else if (document.getElementById('clue').value == 2) { // feature
        document.getElementById("demo").innerHTML = '<br><p>1</p>';
        // fetch
      } else if (document.getElementById('clue').value == 3) { // card
        document.getElementById("demo").innerHTML = '<br><p>1</p>';
        // fetch
      } else if (document.getElementById('clue').value == 4) { // image
        document.getElementById("demo").innerHTML = '<br><p>1</p>';
        // fetch
      } else if (document.getElementById('clue').value == 5) { // content
        document.getElementById("demo").innerHTML = '<br><p>1</p>';
        // fetch
      }
  }

    return (
    	<>
        <Container>
          <Row className='justify-content-md-center'>
            <h1>Form Contents</h1>
          </Row>
          <Row className='justify-content-md-center'>
            <Form.Control as='select' id='options' onChange={this.handleDropdownChange}>
              <option value='1'>1</option>
              <option value='2'>2</option>
              <option value='3'>3</option>
            </Form.Control>
          </Row>

          <Row id='demo'>id demo</Row>
          <div id='forms'>id form</div>

          <Form.Control type='hidden' id='clue' name='clue' value={this.state.selectValue} />
          <Button variant="success" onClick={submit}>Submit</Button>{' '}
          <br />

          // quote
          <Row className='justify-content-md-center'>

            <Form.Label><h4>Quote</h4></Form.Label>
            <Form.Control as='textarea' rows={3} id='quote' name='quote' />

            <Form.Label><h4>By</h4></Form.Label>
            <Form.Control type='text' id='by' name='by' />

            <Form.Label><h4>Type</h4></Form.Label>
            <Form.Control as='select' id='type' name='type' onChange={this.handleDropdownChange}>
              <option value='atas'>Atas</option>
              <option value='bawah'>Bawah</option>
            </Form.Control>

            <br /><br /><br />
            <Form.Control type='hidden' id='clue' name='clue' value={this.state.selectValue} />
            <Button variant="primary" onClick={submit}>Submit</Button>{' '}
          </Row>

          // feature
          <Row className='justify-content-md-center'>

            <Form.Label><h4>Icon</h4></Form.Label>
            <Form.Control as='select' id='icon' name='icon' onChange={this.handleDropdownChange}>
              <option value='faHome'>Home</option>
              <option value='faHeart'>Heart</option>
            </Form.Control>

            <Form.Label><h4>Title</h4></Form.Label>
            <Form.Control type='text' id='title' name='title' />

            <Form.Label><h4>Subtitle</h4></Form.Label>
            <Form.Control type='text' id='subtitle' name='subtitle' />

            <br /><br /><br />
            <Form.Control type='hidden' id='clue' name='clue' value={this.state.selectValue} />
            <Button variant="primary" onClick={submit}>Submit</Button>{' '}
          </Row>

          // card
          <Row className='justify-content-md-center'>

            <Form.Label><h4>Title</h4></Form.Label>
            <Form.Control type='text' id='title' name='title' />

            <Form.Label><h4>Subtitle</h4></Form.Label>
            <Form.Control as='textarea' rows={3} id='subtitle' name='subtitle' />

            <br /><br /><br />
            <Form.Control type='hidden' id='clue' name='clue' value={this.state.selectValue} />
            <Button variant="primary" onClick={submit}>Submit</Button>{' '}
          </Row>

          // image
          <Row className='justify-content-md-center'>

            <Form.Label><h4>Image URL</h4></Form.Label>
            <Form.Control type='text' id='imgUrl' name='imgUrl' />

            <Form.Label><h4>Type</h4></Form.Label>
            <Form.Control as='select' id='type' name='type' onChange={this.handleDropdownChange}>
              <option value='atas'>Atas</option>
              <option value='bawah'>Bawah</option>
            </Form.Control>

            <br /><br /><br />
            <Form.Control type='hidden' id='clue' name='clue' value={this.state.selectValue} />
            <Button variant="primary" onClick={submit}>Submit</Button>{' '}
          </Row>

          // Content
          <Row className='justify-content-md-center'>

            <Form.Label><h4>Title</h4></Form.Label>
            <Form.Control as='textarea' rows={3} id='title' name='title' />

            <Form.Label><h4>Subtitle</h4></Form.Label>
            <Form.Control type='text' id='subtitle' name='subtitle' />

            <Form.Label><h4>Type</h4></Form.Label>
            <Form.Control as='select' id='type' name='type' onChange={this.handleDropdownChange}>
              <option value='atas'>Atas</option>
              <option value='bawah'>Bawah</option>
            </Form.Control>

            <br /><br /><br />
            <Form.Control type='hidden' id='clue' name='clue' value={this.state.selectValue} />
            <Button variant="primary" onClick={submit}>Submit</Button>{' '}
          </Row>

        </Container>
        
        
      </>
    );
  }


}

export default FormComponent;
