import React, { Component } from "react";
import "./FormAlvin.css";
import { Container } from "react-bootstrap";
import { Button, Row, Col } from "react-bootstrap";
import ImgForm from "../../components/Form/ImgForm";
import ContentForm from "../../components/Form/ContentForm";
import CardForm from "../../components/Form/CardForm";
import FeatureForm from "../../components/Form/FeatureForm";
import QuoteForm from "../../components/Form/QuoteForm";

export default class FormAlvin extends Component {
  constructor() {
    super();
    this.state = {
      ShowHideImgForm: false,
      ShowHideContentForm: false,
      ShowHideCardForm: false,
      ShowHideFeatureForm: false,
      ShowHideQuoteForm: false,
    };
  }

  showHide(name) {
    switch (name) {
      case "imgForm":
        this.setState({ ShowHideImgForm: !this.state.ShowHideImgForm });
        this.setState({ ShowHideContentForm: false });
        this.setState({ ShowHideCardForm: false });
        this.setState({ ShowHideFeatureForm: false });
        this.setState({ ShowHideQuoteForm: false });
        break;
      case "contentForm":
        this.setState({ ShowHideContentForm: !this.state.ShowHideContentForm });
        this.setState({ ShowHideImgForm: false });
        this.setState({ ShowHideCardForm: false });
        this.setState({ ShowHideFeatureForm: false });
        this.setState({ ShowHideQuoteForm: false });
        break;
      case "cardForm":
        this.setState({ ShowHideCardForm: !this.state.ShowHideCardForm });
        this.setState({ ShowHideFeatureForm: false });
        this.setState({ ShowHideQuoteForm: false });
        this.setState({ ShowHideImgForm: false });
        this.setState({ ShowHideContentForm: false });
        break;
      case "FeatureForm":
        this.setState({ ShowHideFeatureForm: !this.state.ShowHideFeatureForm });
        this.setState({ ShowHideQuoteForm: false });
        this.setState({ ShowHideCardForm: false });
        this.setState({ ShowHideImgForm: false });
        this.setState({ ShowHideContentForm: false });
        break;

      case "QuoteForm":
        this.setState({ ShowHideQuoteForm: !this.state.ShowHideQuoteForm });
        this.setState({ ShowHideFeatureForm: false });
        this.setState({ ShowHideImgForm: false });
        this.setState({ ShowHideContentForm: false });
        this.setState({ ShowHideCardForm: false });
        break;

      default:
        break;
    }
  }

  postData = () => {
    let options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    };

    if (this.state.ShowHideImgForm) {
      let data = {
        imgUrl: document.getElementById("imgUrl").value,
        type: document.getElementById("imgType").value,
      };
      options.body = JSON.stringify(data);

      fetch("http://localhost:8081/saveImage", options);
    }

    if (this.state.ShowHideContentForm) {
      let data = {
        title: document.getElementById("contentTitle").value,
        text: document.getElementById("contentText").value,
        type: document.getElementById("contentType").value,
      };
      options.body = JSON.stringify(data);
      fetch("http://localhost:8081/savecontent", options);
    }

    if (this.state.ShowHideCardForm) {
      let data = {
        title: document.getElementById("cardTitle").value,
        subtitle: document.getElementById("cardSubTitle").value,
      };
      options.body = JSON.stringify(data);
      fetch("http://localhost:8081/savecard", options);
    }

    if (this.state.ShowHideFeatureForm) {
      let data = {
        icon: document.getElementById("featureIcon").value,
        title: document.getElementById("featureTitle").value,
        subtitle: document.getElementById("featureSubTitle").value,
      };
      options.body = JSON.stringify(data);
      fetch("http://localhost:8081/saveFeature", options);
    }

    if (this.state.ShowHideQuoteForm) {
      let data = {
        quote: "",
        by: "",
        type: "",
      };
      options.body = JSON.stringify(data);
      fetch("http://localhost:8081/saveQuote", options);
    }
  };

  render() {
    return (
      <>
        <Container id="form">
          <Row>
            <Col md={2}>
              <Button onClick={() => this.showHide("imgForm")}>ImgForm</Button>
            </Col>
            <Col md={2}>
              <Button onClick={() => this.showHide("contentForm")}>
                ContentForm
              </Button>
            </Col>
            <Col md={2}>
              <Button onClick={() => this.showHide("cardForm")}>
                CardForm
              </Button>
            </Col>
            <Col md={2}>
              <Button onClick={() => this.showHide("FeatureForm")}>
                FeatureForm
              </Button>
            </Col>
            <Col md={2}>
              <Button onClick={() => this.showHide("QuoteForm")}>
                QuoteForm
              </Button>
            </Col>
          </Row>
          {this.state.ShowHideImgForm && <ImgForm />}
          {this.state.ShowHideContentForm && <ContentForm />}
          {this.state.ShowHideCardForm && <CardForm />}
          {this.state.ShowHideFeatureForm && <FeatureForm />}
          {this.state.ShowHideQuoteForm && <QuoteForm />}

          {(this.state.ShowHideImgForm ||
            this.state.ShowHideContentForm ||
            this.state.ShowHideCardForm ||
            this.state.ShowHideFeatureForm ||
            this.state.ShowHideQuoteForm) && (
            <Button onClick={this.postData}>Submit</Button>
          )}
        </Container>
      </>
    );
  }
}
