import React, { Component } from "react";
import Header from "./../components/Header/Header";
import SubHeader from "./../components/SubHeader/SubHeader";
import Card from "./../components/Card/Card";
import { Container } from "react-bootstrap";
import ImgSlider from "./../components/ImgSlider/ImgSlider";

import FooterComponent from "./../components/FooterComponent";

import QuoteComponent from "../components/QuoteComponent";
import FeaturesComponent from "../components/FeaturesComponent";

export default class Home extends Component {
  render() {
    return (
      <>
        <ImgSlider />
        <SubHeader />
        <Container className="mt-5 mb-5" style={{ width: "960px" }}>
          {" "}
          <FeaturesComponent />
          <Card />
          <QuoteComponent page='home' />
        </Container>
      </>
    );
  }
}
