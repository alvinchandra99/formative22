package com.nexsoft.formative22.controller;

import java.util.List;

import com.nexsoft.formative22.model.Card;
import com.nexsoft.formative22.model.Content;
import com.nexsoft.formative22.model.Quotes;
import com.nexsoft.formative22.service.CardService;
import com.nexsoft.formative22.service.ContentService;
import com.nexsoft.formative22.service.QuotesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/")
public class ApiController {

    @Autowired
    CardService cardService;

    @Autowired
    ContentService contentService;

    @Autowired
    QuotesService quotesService;
    

    @GetMapping("/getcard")
    public List<Card> getAllCard(){
        List<Card> listCard = cardService.findAllCard();
        return listCard;
    }

    @PostMapping("/savecard")
    public void saveCard(@RequestBody Card card){
        System.out.println(card.getSubtitle());
        cardService.save(card);
    }

    @GetMapping("getcontent")
    public List<Content>getContentByType(@RequestParam String type){
        return contentService.findByType(type);
    }

    @PostMapping("/savecontent")
    public void saveContent(@RequestBody Content content){
        contentService.save(content);
    }

    @GetMapping("/getquote/{pages}")
    public List<Quotes> getQuote(@RequestBody String pages){
        return quotesService.findByType(pages);
    }

    @PostMapping("/savequotes")
    public void save(Quotes quotes){
        quotesService.save(quotes);
    }
    
    
}
