package com.nexsoft.formative22.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nexsoft.formative22.model.Feature;
import com.nexsoft.formative22.model.Image;
import com.nexsoft.formative22.repository.FeatureRepository;
import com.nexsoft.formative22.repository.ImageRepository;
import com.nexsoft.formative22.service.ImageService;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/")
public class GeneralController {
	@Autowired
	FeatureRepository featureRepo;
	@Autowired
	ImageRepository imageRepo;
	@Autowired
    ImageService imageService;
	
	@GetMapping("/getFeatures")
	public List<Feature> getAllFeature() {
		return featureRepo.findAll();
	}
	
	@GetMapping("/feature/{id}")
	public Feature getByIdFeature(@RequestBody int id) {
		return featureRepo.findById(id);
	}
	
	@PostMapping("/saveFeature")
    public void saveFeature(@RequestBody Feature feature){
		featureRepo.save(feature);
    }
	
	@GetMapping("/images")
	public List<Image> getAllImage() {
		System.out.println("test");
		return imageRepo.findAll();
	}
	
	@GetMapping("/image/{id}")
	public Image getByIdImage(@RequestBody int id) {
		return imageRepo.findById(id);
	}
	
	@GetMapping("getimages")
	public List<Image> getImageByType(@RequestParam String type) {
		return imageService.findByType(type);
	}
	
	@PostMapping("/saveImage")
    public void saveImage(@RequestBody Image image){
		imageRepo.save(image);
    }
}
