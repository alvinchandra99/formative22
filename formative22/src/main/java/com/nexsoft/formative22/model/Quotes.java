package com.nexsoft.formative22.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Quotes implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String quote;

    private String quoteby;

    private String type;

    public Quotes() {
    }

    public Quotes(int id, String quote, String quoteby, String type) {
        this.id = id;
        this.quote = quote;
        this.quoteby = quoteby;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getQuoteby() {
        return quoteby;
    }

    public void setQuoteby(String quoteby) {
        this.quoteby = quoteby;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

   
    
    
}
