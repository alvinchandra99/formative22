package com.nexsoft.formative22.repository;

import java.util.List;

import com.nexsoft.formative22.model.Card;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends CrudRepository<Card, Integer> {

    List<Card> findAll();
    
}
