package com.nexsoft.formative22.repository;

import java.util.List;

import com.nexsoft.formative22.model.Content;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContentRepository extends CrudRepository<Content, Integer> {
	
    List<Content> findByType(String type);

}
