package com.nexsoft.formative22.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.nexsoft.formative22.model.Feature;

public interface FeatureRepository extends CrudRepository<Feature, Integer> {
	Feature findById(int id);
    List<Feature> findAll();
}
