package com.nexsoft.formative22.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.nexsoft.formative22.model.Image;

public interface ImageRepository extends CrudRepository<Image, Integer> {
    Image findById(int id);
    List<Image> findAll();
    List<Image> findByType(String type);
}