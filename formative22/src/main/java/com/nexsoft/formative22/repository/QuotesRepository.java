package com.nexsoft.formative22.repository;

import java.util.List;

import com.nexsoft.formative22.model.Quotes;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuotesRepository extends CrudRepository<Quotes, Integer> {

    List<Quotes> findByType(String type);

}
