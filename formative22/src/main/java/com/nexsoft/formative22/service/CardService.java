package com.nexsoft.formative22.service;

import java.util.List;

import javax.transaction.TransactionScoped;

import com.nexsoft.formative22.model.Card;
import com.nexsoft.formative22.repository.CardRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class CardService {

    @Autowired
    CardRepository cardRepository;


    public List<Card> findAllCard(){
        return cardRepository.findAll();
    }

    public void save(Card card){
        cardRepository.save(card);
    }

    
}
