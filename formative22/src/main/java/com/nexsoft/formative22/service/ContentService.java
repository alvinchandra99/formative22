package com.nexsoft.formative22.service;

import java.util.List;

import javax.transaction.TransactionScoped;

import com.nexsoft.formative22.model.Content;
import com.nexsoft.formative22.repository.ContentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class ContentService {
    @Autowired
    ContentRepository contentRepository;

    public List<Content> findByType(String type){
        return contentRepository.findByType(type) ;
    }

    public void save(Content content){
        contentRepository.save(content);
    }
}
