package com.nexsoft.formative22.service;

import java.util.List;

import javax.transaction.TransactionScoped;

import com.nexsoft.formative22.model.Content;
import com.nexsoft.formative22.model.Image;
import com.nexsoft.formative22.repository.ContentRepository;
import com.nexsoft.formative22.repository.ImageRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class ImageService {
    @Autowired
    ImageRepository imageRepository;

    public List<Image> findByType(String type){
        return imageRepository.findByType(type) ;
    }
    
    public List<Image> findAll(){
        return imageRepository.findAll() ;
    }

    public void save(Image content){
    	imageRepository.save(content);
    }
}
