package com.nexsoft.formative22.service;

import java.util.List;

import javax.transaction.TransactionScoped;

import com.nexsoft.formative22.model.Quotes;
import com.nexsoft.formative22.repository.QuotesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class QuotesService {

    @Autowired
    QuotesRepository quotesRepository;

    public void save(Quotes quotes){
        quotesRepository.save(quotes);
    }

    public List<Quotes> findByType(String type){
       return  quotesRepository.findByType(type);
    }
    
}
